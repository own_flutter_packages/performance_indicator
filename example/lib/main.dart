import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:performance_indicator/performance_indicator.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Example for performance indicator',
      home: const ExampleApp(),
      themeMode: ThemeMode.dark,
      theme: ThemeData(brightness: Brightness.dark),
      color: Colors.black,
    ),
  );
}

class ExampleApp extends StatelessWidget {
  static const String exampleContentId = 'asdf';

  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final quality = await PerformanceIndicator.getCurrentQualityPreference();
      if (quality != null) {
        /// You can retrieve the stored information about quality preference by content_id.
        final content = await PerformanceIndicator.getContent(exampleContentId);

        if (content == null) {
          /// You can add information about the quality preference of your content.
          await PerformanceIndicator.addContent(
            exampleContentId,
            quality,
          );
        }
      }

      /// You can start current speed check.
      /// This will influence the color of performance indicator pulsing button.
      await PerformanceIndicator.checkCurrentSpeed(
        badResponseTime: 500,
        url: 'https://google.com',
      );
    });

    /// or just return PerformanceIndicator.exampleApp(width: size.width, height: size.height),
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            /// This container could be your media content.
            child: Container(
              height: size.height * 0.2,
              width: size.width * 0.75,
              color: Colors.black38,
              alignment: Alignment.topRight,
              padding: const EdgeInsets.all(10),

              /// Quality converter can be placed inside of your media widget.
              ///
              /// Here we use exampleContentId. Please add content manually as shown in WidgetsBinding function above.
              child: PerformanceIndicator.qualityConverter(
                width: size.width * 0.1,
                contentId: exampleContentId,
                onChangedQuality: _onChangedContentQuality,
              ),
            ),
          ),
          Center(
            /// This is a content widget with a build-in quality converter.
            ///
            /// You can align quality converter relatively to your content.
            /// If you do not specify any content_id it will be generated.
            child: PerformanceIndicator.contentWithQualityConverter(
              child: Container(
                height: size.height * 0.2,
                width: size.width * 0.75,
                color: Colors.black54,
              ),
              qualityConverterWidth: size.width * 0.1,
              qualityConverterAlignment: const Alignment(0.9, 0.9),
              onCreated: _onCreatedContentId,
              onChangedContentQuality: _onChangedContentQuality,
            ),
          ),
          Center(
            /// Performance indicator can be used placed everywhere
            /// as e.g. an already defined with quality dialog options.
            child: PerformanceIndicator.quality(
              width: size.width * 0.25,
              checkUri: Uri(host: 'google.com', scheme: 'https'),
              badResponseTimeInMs: 300,
            ),
          ),
        ],
      ),
    );
  }

  _onCreatedContentId(String contentId) {
    if (kDebugMode) {
      print('Created content ID $contentId automatically.');
    }
  }

  _onChangedContentQuality(
      String contentId, QualityPreference qualityPreference) {
    if (kDebugMode) {
      print('Updated content ID $contentId with quality: $qualityPreference.');
    }
  }
}
