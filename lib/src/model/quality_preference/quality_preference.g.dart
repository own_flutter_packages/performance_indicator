// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quality_preference.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class QualityPreferenceAdapter extends TypeAdapter<QualityPreference> {
  @override
  final int typeId = 222;

  @override
  QualityPreference read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return QualityPreference.low;
      case 1:
        return QualityPreference.medium;
      case 2:
        return QualityPreference.high;
      default:
        return QualityPreference.low;
    }
  }

  @override
  void write(BinaryWriter writer, QualityPreference obj) {
    switch (obj) {
      case QualityPreference.low:
        writer.writeByte(0);
        break;
      case QualityPreference.medium:
        writer.writeByte(1);
        break;
      case QualityPreference.high:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is QualityPreferenceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
