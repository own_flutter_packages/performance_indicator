import 'package:hive_flutter/hive_flutter.dart';

part 'quality_preference.g.dart';

@HiveType(typeId: 222)
enum QualityPreference {
  @HiveField(0)
  low,
  @HiveField(1)
  medium,
  @HiveField(2)
  high,
}
