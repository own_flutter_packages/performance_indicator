// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'speed_details.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SpeedDetailsAdapter extends TypeAdapter<SpeedDetails> {
  @override
  final int typeId = 223;

  @override
  SpeedDetails read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SpeedDetails(
      timestamp: fields[0] as int,
      responseTime: fields[1] as int,
      checkUrl: fields[2] as String,
      qualityPreference: fields[3] as QualityPreference,
      speedQuality: fields[4] as double,
      responseTimePing: fields[5] as int,
    );
  }

  @override
  void write(BinaryWriter writer, SpeedDetails obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.timestamp)
      ..writeByte(1)
      ..write(obj.responseTime)
      ..writeByte(2)
      ..write(obj.checkUrl)
      ..writeByte(3)
      ..write(obj.qualityPreference)
      ..writeByte(4)
      ..write(obj.speedQuality)
      ..writeByte(5)
      ..write(obj.responseTimePing);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SpeedDetailsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
