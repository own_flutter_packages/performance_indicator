import 'package:hive_flutter/hive_flutter.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/services/speed_service.dart';

part 'speed_details.g.dart';

@HiveType(typeId: 223)
class SpeedDetails extends HiveObject {
  SpeedDetails({
    required this.timestamp,
    required this.responseTime,
    required this.checkUrl,
    required this.qualityPreference,
    required this.speedQuality,
    required this.responseTimePing,
  });

  @HiveField(0)
  final int timestamp;
  @HiveField(1)
  final int responseTime;
  @HiveField(2)
  final String checkUrl;
  @HiveField(3)
  final QualityPreference qualityPreference;
  @HiveField(4)
  final double speedQuality;
  @HiveField(5)
  final int responseTimePing;

  static SpeedDetails of({
    required int responseTimeUri,
    required String checkUrl,
    required QualityPreference qualityPreference,
    required int responseTimePing,
    int? badResponseTime,
    bool isResultEmpty = false,
  }) {
    return SpeedDetails(
      timestamp: DateTime.now().millisecondsSinceEpoch,
      responseTime: responseTimeUri,
      responseTimePing: responseTimePing,
      checkUrl: checkUrl,
      qualityPreference: qualityPreference,
      speedQuality: !isResultEmpty
          ? SpeedService.calcSpeedQuality(
              responseTimeUri: responseTimeUri,
              responseTimePing: responseTimePing,
              badResponseTime: badResponseTime,
            )
          : 0.0,
    );
  }

  @override
  String toString() {
    final date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    final responseTime = this.responseTime / 1000;
    final responseTimePing = this.responseTimePing / 1000;
    final speedQuality = this.speedQuality * 100;

    return '[$checkUrl] uri($responseTime ms) ping($responseTimePing ms) quality(${speedQuality.toStringAsPrecision(2)}%) date:$date';
  }
}
