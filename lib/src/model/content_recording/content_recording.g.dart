// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'content_recording.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ContentRecordingAdapter extends TypeAdapter<ContentRecording> {
  @override
  final int typeId = 221;

  @override
  ContentRecording read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ContentRecording(
      contentId: fields[0] as String,
      qualityPreference: fields[1] as QualityPreference,
      lastUpdate: fields[2] as int,
      notAccessible: fields[3] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, ContentRecording obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.contentId)
      ..writeByte(1)
      ..write(obj.qualityPreference)
      ..writeByte(2)
      ..write(obj.lastUpdate)
      ..writeByte(3)
      ..write(obj.notAccessible);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ContentRecordingAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
