import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/services/content_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/services/speed_service.dart';
import 'package:performance_indicator/src/widgets/content/content_with_changing_quality.dart';
import 'package:performance_indicator/src/widgets/performance_indicator/performance_indicator_widget.dart';
import 'package:performance_indicator/src/widgets/quality_converter/quality_converter.dart';

import '../widgets/example_app/example_app.dart';

/// Not part of public API
class PerformanceIndicatorImpl implements PerformanceIndicatorInterface {
  /// Not part of public API
  PerformanceIndicatorImpl();

  static PerformanceService? _performanceService;
  static SpeedService? _speedService;
  static ContentService? _contentService;

  Future<PerformanceService> get performanceService async {
    if (_performanceService != null) return _performanceService!;

    try {
      _performanceService = await PerformanceService.instance();
    } catch (ex) {
      if (kDebugMode) {
        print(ex.toString());
      }
      _performanceService = await PerformanceService.instance();
    }

    return _performanceService!;
  }

  Future<SpeedService> get speedService async {
    if (_speedService != null) return _speedService!;

    try {
      _speedService = await SpeedService.instance();
    } catch (ex) {
      if (kDebugMode) {
        print(ex.toString());
      }
      _speedService = await SpeedService.instance();
    }

    return _speedService!;
  }

  Future<ContentService> get contentService async {
    if (_contentService != null) return _contentService!;

    try {
      _contentService = await ContentService.instance();
    } catch (ex) {
      if (kDebugMode) {
        print(ex.toString());
      }
      _contentService = await ContentService.instance();
    }

    return _contentService!;
  }

  @override
  PerformanceIndicatorWidget custom({
    required double width,
    required List<DialogOptionDetails> dialogOptions,
    int? badResponseTimeInMs,
    Uri? checkUrl,
    String? title,
  }) {
    return PerformanceIndicatorWidget.custom(
      width: width,
      dialogOptions: dialogOptions,
      badResponseTimeInMs: badResponseTimeInMs,
      checkUrl: checkUrl,
      title: title,
    );
  }

  @override
  PerformanceIndicatorWidget quality({
    required double width,
    int? badResponseTimeInMs,
    Uri? checkUri,
    String? title,
  }) {
    return PerformanceIndicatorWidget.quality(
      width: width,
      badResponseTimeInMs: badResponseTimeInMs,
      checkUri: checkUri,
      title: title,
    );
  }

  @override
  QualityConverter qualityConverter({
    required double width,
    required String contentId,
    String? title,
    Function(String, QualityPreference)? onChangedQuality,
  }) {
    return QualityConverter.embeddable(
      title: title,
      width: width,
      contentId: contentId,
      onChangedQuality: onChangedQuality,
    );
  }

  @override
  ContentWithChangingQuality contentWithQualityConverter({
    required Widget child,
    String? contentId,
    Alignment qualityConverterAlignment = Alignment.topRight,
    double? qualityConverterWidth,
    String? qualityConverterTitle,
    Function(String)? onCreated,
    Function(String, QualityPreference)? onChangedContentQuality,
  }) {
    return ContentWithChangingQuality.withQualityConverter(
      child: child,
      contentId: contentId,
      qualityConverterAlignment: qualityConverterAlignment,
      qualityConverterWidth: qualityConverterWidth,
      qualityConverterTitle: qualityConverterTitle,
      onCreated: onCreated,
      onChangedContentQuality: onChangedContentQuality,
    );
  }

  @override
  Future<T?> checkCurrentSpeed<T>({
    Future<T>? request,
    String? url,
    int? badResponseTime,
    int? timeoutMs,
  }) async {
    return (await speedService).checkCurrentSpeed(
      request: request,
      url: url,
      badResponseTime: badResponseTime,
      timeoutMs: timeoutMs,
    );
  }

  @override
  Future<ContentRecording> addContent(
    String contentId,
    QualityPreference qualityPreference, {
    bool? notAccessible = false,
  }) async {
    return (await contentService).createContentRecording(
      ContentRecording(
        contentId: contentId,
        qualityPreference: qualityPreference,
        notAccessible: notAccessible,
        lastUpdate: DateTime.now().millisecondsSinceEpoch,
      ),
    );
  }

  @override
  Future<ContentRecording?> getContent(String contentId) async {
    return (await contentService).getByContentId(contentId);
  }

  @override
  Future<ContentRecording> updatedContentQuality(
    String contentId,
    QualityPreference qualityPreference,
  ) async {
    return (await contentService).updateQualityPreference(
      contentId,
      qualityPreference,
    );
  }

  @override
  Future<bool> markContentAsNotAccessible(String contentId) async {
    return (await contentService).markContentAsNotAccessible(contentId);
  }

  @override
  Future<void> updateCurrentSpeed(SpeedDetails speedDetails) async {
    return (await speedService).updateCurrentSpeed(speedDetails);
  }

  @override
  Future<void> updateQuality(QualityPreference preference) async {
    return (await performanceService).updateQuality(preference);
  }

  @override
  Future<void> deleteRecording(SpeedDetails speedDetails) async {
    return (await speedService).deleteRecording(speedDetails);
  }

  @override
  Future<bool> existsAnyQualityPreference() async {
    return (await performanceService).exists();
  }

  @override
  Future<SpeedDetails?> getLastCheckedSpeed() async {
    return (await speedService).getLastCheckedSpeed();
  }

  @override
  Future<QualityPreference?> getCurrentQualityPreference() async {
    return (await performanceService).getQualityPreference();
  }

  @override
  QualityPreference? getCurrentQualityPreferenceSync() {
    return PerformanceService.instanceSync().getQualityPreference();
  }

  @override
  Future<void> removeOldRecordings() async {
    return (await speedService).removeOldRecordings();
  }

  @override
  ExampleApp exampleApp(Size size) {
    return ExampleApp(
      width: size.width,
      height: size.height,
    );
  }
}
