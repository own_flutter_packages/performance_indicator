import 'package:hive_flutter/adapters.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/repo/db/hive_db.dart';

class PerformanceService {
  static final PerformanceService _singleton = PerformanceService._internal();

  // ignore: unused_element
  factory PerformanceService._() {
    return _singleton;
  }

  PerformanceService._internal();

  static HiveDB? hiveDb;

  static Future<PerformanceService> instance() async {
    hiveDb = await HiveDB.instance();

    return _singleton;
  }

  static PerformanceService instanceSync() {
    return _singleton;
  }

  Future<void> updateQuality(QualityPreference preference) async {
    if (exists()) {
      final previouslyStored = _db()?.getAt(0);

      if (previouslyStored != null) {
        await _db()?.clear();
      }
    }

    await _db()?.add(
      {
        'qualityPreference': preference,
        'edited': DateTime.now(),
      },
    );
  }

  bool exists() {
    final previouslyStored =
        (_db()?.isNotEmpty ?? false) ? _db()?.getAt(0) : null;

    if (previouslyStored != null) {
      return true;
    }

    return false;
  }

  QualityPreference? getQualityPreference() {
    final stored = (_db()?.isNotEmpty ?? false) ? _db()?.getAt(0) : null;

    if (stored != null) {
      final qualityPreference = stored['qualityPreference'];

      if (qualityPreference != null) {
        return qualityPreference;
      }
    }

    return null;
  }

  static QualityPreference? getQualityFromBox(Box box) {
    if (box.isNotEmpty && box.getAt(0) != null) {
      return box.getAt(0)['qualityPreference'];
    }

    return null;
  }

  static const String dbName = 'quality_preferences';

  static Box<dynamic>? _db() {
    return hiveDb?.db(dbName);
  }
}
