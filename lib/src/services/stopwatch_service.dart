import 'package:flutter/foundation.dart';
import 'package:http/http.dart';

class StopwatchService {
  static final StopwatchService _singleton = StopwatchService._internal();

  factory StopwatchService() {
    return _singleton;
  }

  StopwatchService._internal();

  StopwatchService init() {
    return _singleton;
  }

  static Future<T?> async<T>(
    Future<T?> future, {
    String? name,
    Function(int?, {required bool isResultEmpty})? whenCompleted,
    bool micro = false,
    bool log = false,
  }) async {
    final stopwatch = Stopwatch();
    stopwatch.start();
    T? result;

    try {
      result = await future;
    } catch (e) {
      if (kDebugMode) {
        if (e is Response) {
          print('Error when making async with response code: ${e.statusCode}');
        } else {
          print('Error when making async request: ${e.toString()}');
        }
      }
    }

    stopwatch.stop();

    final elapsedTime = micro
        ? stopwatch.elapsed.inMicroseconds
        : stopwatch.elapsed.inMilliseconds;

    if (whenCompleted != null) {
      whenCompleted(
        elapsedTime,
        isResultEmpty: result == null,
      );
    }

    if (log) {
      if (kDebugMode) {
        print(
          '[STOPWATCH] - execute async $name | $elapsedTime ${micro ? 'mms' : 'ms'}.',
        );
      }
    }

    return result;
  }

  static T? sync<T>(
    Function function, {
    String? name,
    Function(int)? whenCompleted,
    bool micro = false,
    bool log = false,
  }) {
    final stopwatch = Stopwatch();
    stopwatch.start();
    T? result;

    try {
      result = function();
    } catch (e) {
      if (kDebugMode) {
        print('Error when making sync request: ${e.toString()}');
      }
    }

    stopwatch.stop();

    final elapsedTime = micro
        ? stopwatch.elapsed.inMicroseconds
        : stopwatch.elapsed.inMilliseconds;

    if (whenCompleted != null) {
      whenCompleted(elapsedTime);
    }

    if (log) {
      if (kDebugMode) {
        print(
            '[STOPWATCH] - execute sync $name  | $elapsedTime ${micro ? 'mms' : 'ms'}.');
      }
    }

    return result;
  }
}
