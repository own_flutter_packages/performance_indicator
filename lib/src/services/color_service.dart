import 'dart:math';

import 'package:flutter/material.dart';

class ColorService {
  static final ColorService _singleton = ColorService._internal();

  factory ColorService() {
    return _singleton;
  }

  ColorService._internal();

  ColorService init() {
    return _singleton;
  }

  static const Color nintyBlack = Color.fromRGBO(0, 0, 0, 1);
  static const Color nintyGreyLight = Color.fromRGBO(223, 218, 224, 1);
  static const Color nintyGrey = Color.fromRGBO(190, 190, 199, 1);
  static const Color nintyAubergine = Color.fromRGBO(19, 22, 38, 1);

  static const Color nintyGreen = Color.fromRGBO(32, 230, 139, 1);
  static const Color nintyTurkis = Color.fromRGBO(119, 242, 242, 1);
  static const Color nintyTurkisDark = Color.fromRGBO(54, 105, 115, 1);
  static const Color nintyTurkisDarkest = Color.fromRGBO(10, 30, 41, 1);
  static const Color nintyYellow = Color.fromRGBO(255, 210, 74, 1);
  static const Color nintyRed = Color.fromRGBO(255, 109, 99, 1);
  static const Color nintyViolett = Color.fromRGBO(255, 0, 184, 1);

  static const Color backgroundPrimary = nintyBlack;
  static const Color backgroundSecondary = nintyAubergine;
  static const Color fontPrimary = nintyGreyLight;
  static const Color fontPrimaryUnfocused = nintyGrey;
  static const Color fontSecondary = nintyAubergine;
  static const Color fontSecondaryUnfocused = nintyAubergine;
  static const Color accentPrimary = nintyYellow;
  static const Color accentSecondary = nintyRed;
  static const Color selectedPrimary = nintyTurkis;
  static const Color selectedSecondary = nintyGreen;

  static Color randomColorFromNinty() {
    final Random random = Random();
    final index = random.nextInt(6);
    final colors = [
      ColorService.accentSecondary,
      ColorService.nintyGreyLight,
      ColorService.accentPrimary,
      ColorService.selectedSecondary,
      ColorService.selectedPrimary,
      ColorService.nintyViolett,
    ];

    return colors[index];
  }

  static Color lighten(Color color, [double amount = .1]) {
    assert(amount >= 0 && amount <= 1);

    final hsl = HSLColor.fromColor(color);
    final hslLight =
        hsl.withLightness((hsl.lightness + amount).clamp(0.0, 1.0));

    return hslLight.toColor();
  }

  static Color darken(Color color, [double amount = .1]) {
    assert(amount >= 0 && amount <= 1);

    final hsl = HSLColor.fromColor(color);
    final hslDark = hsl.withLightness((hsl.lightness - amount).clamp(0.0, 1.0));

    return hslDark.toColor();
  }
}
