import 'package:align_positioned/align_positioned.dart';
import 'package:flutter/cupertino.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/services/content_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/widgets/quality_converter/quality_converter.dart';
import 'package:uuid/uuid.dart';

class ContentWithChangingQuality extends StatefulWidget {
  const ContentWithChangingQuality._({
    Key? key,
    required this.child,
    this.contentId,
    required this.qualityConverterWidth,
    this.alignmentQualityConverter = Alignment.topRight,
    this.qualityConverterTitle,
    this.onCreated,
    this.onChangedContentQuality,
  }) : super(key: key);

  final Widget child;
  final String? contentId;
  final double qualityConverterWidth;
  final Alignment alignmentQualityConverter;
  final String? qualityConverterTitle;
  final Function(String)? onCreated;
  final Function(String, QualityPreference)? onChangedContentQuality;

  static ContentWithChangingQuality withQualityConverter({
    required Widget child,
    String? contentId,
    Alignment qualityConverterAlignment = Alignment.topRight,
    double? qualityConverterWidth,
    String? qualityConverterTitle,
    Function(String)? onCreated,
    Function(String, QualityPreference)? onChangedContentQuality,
  }) {
    return ContentWithChangingQuality._(
      contentId: contentId,
      alignmentQualityConverter: qualityConverterAlignment,
      qualityConverterWidth: qualityConverterWidth ?? 100,
      qualityConverterTitle: qualityConverterTitle,
      onCreated: onCreated,
      onChangedContentQuality: onChangedContentQuality,
      child: child,
    );
  }

  @override
  State<ContentWithChangingQuality> createState() =>
      _ContentWithChangingQualityState();
}

class _ContentWithChangingQualityState
    extends State<ContentWithChangingQuality> {
  late Future<String> future;

  Future<String> getData() async {
    final contentService = await ContentService.instance();
    final performanceService = await PerformanceService.instance();

    if (widget.contentId != null) {
      final content = contentService.getByContentId(widget.contentId!);
      final quality = performanceService.getQualityPreference();

      if (content == null) {
        await contentService.createContentRecording(
          ContentRecording(
            contentId: widget.contentId!,
            qualityPreference: quality ?? QualityPreference.high,
            lastUpdate: DateTime.now().millisecondsSinceEpoch,
          ),
        );
      }

      if (widget.onCreated != null) {
        widget.onCreated!(widget.contentId!);
      }

      return widget.contentId!;
    } else {
      final currentQuality = performanceService.getQualityPreference();
      final newContentId = const Uuid().v1();

      await contentService.createContentRecording(
        ContentRecording(
          contentId: newContentId,
          qualityPreference: currentQuality ?? QualityPreference.high,
          lastUpdate: DateTime.now().millisecondsSinceEpoch,
        ),
      );

      if (widget.onCreated != null) {
        widget.onCreated!(newContentId);
      }

      return newContentId;
    }
  }

  @override
  void initState() {
    future = getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
        future: future,
        builder: (context, snapshot) {
          return snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasData
              ? AlignPositioned.relative(
                  container: widget.child,
                  child: QualityConverter.embeddable(
                    width: widget.qualityConverterWidth,
                    contentId: snapshot.data!,
                    title: widget.qualityConverterTitle,
                    onChangedQuality: widget.onChangedContentQuality,
                  ),
                  alignment: widget.alignmentQualityConverter,
                )
              : widget.child;
        });
  }
}
