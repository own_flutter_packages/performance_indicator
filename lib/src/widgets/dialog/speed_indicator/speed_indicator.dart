import 'package:flutter/cupertino.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/speed_service.dart';

class SpeedIndicator extends StatelessWidget {
  const SpeedIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable:
            Hive.box<SpeedDetails>(SpeedService.dbName).listenable(),
        builder: (context, Box box, w) {
          final double speed =
              box.isNotEmpty ? box.values.last.speedQuality : 0;
          final selectedQuality =
              box.isNotEmpty ? box.values.last.qualityPreference : null;

          final double speedTimesHundred = speed * 100;
          final speedString = speedTimesHundred.toStringAsPrecision(3);

          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 4.0),
                child: Icon(
                  MdiIcons.wifi,
                  color: _speedIconColor(speed, selectedQuality),
                  size: 15,
                ),
              ),
              Text(
                'speed: ${speedTimesHundred > 10 ? '$speedString%' : 'very low'}',
                style: _speedTextStyle(speed, selectedQuality),
              ),
            ],
          );
        });
  }

  TextStyle _speedTextStyle(double speed, selectedQuality) => TextStyle(
        fontSize: 13,
        color: _speedIconColor(speed, selectedQuality),
      );

  Color _speedIconColor(double speed, QualityPreference? selectedQuality) =>
      selectedQuality != null
          ? SpeedService.isActionRequired(speed, selectedQuality)
              ? ColorService.accentPrimary
              : ColorService.selectedPrimary
          : ColorService.selectedPrimary;
}
