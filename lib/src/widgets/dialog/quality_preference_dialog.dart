import 'package:flutter/cupertino.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/services/speed_service.dart';
import 'package:performance_indicator/src/widgets/dialog/option/dialog_option.dart';
import 'package:performance_indicator/src/widgets/dialog/speed_indicator/speed_indicator.dart';

class QualityPreferenceDialog extends StatelessWidget {
  static const String defaultTitle = 'Set your quality preferences';
  static const double defaultHeightDialogOption = 75;

  const QualityPreferenceDialog({
    Key? key,
    required this.title,
    required this.dialogOptions,
    required this.closeAction,
    this.withDivider = false,
    this.withPaddingRight = true,
  }) : super(key: key);

  final String title;
  final List<DialogOptionDetails> dialogOptions;
  final DialogOptionDetails closeAction;
  final bool withDivider;
  final bool withPaddingRight;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: StaggeredGrid.count(
        crossAxisCount: 2,
        axisDirection: AxisDirection.down,
        children: [
          StaggeredGridTile.extent(
            crossAxisCellCount: 2,
            mainAxisExtent: 25,
            child: Text(
              title,
              style: _titleTextStyle(),
            ),
          ),
          const StaggeredGridTile.extent(
            crossAxisCellCount: 2,
            mainAxisExtent: 25,
            child: SpeedIndicator(),
          ),
        ],
      ),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: dialogOptions.map((details) {
          return DialogOption(
            details: details,
            height: details.height * 0.75,
            iconColor: details.iconColor,
            withDivider: withDivider,
            withPaddingRight: withPaddingRight,
            onTap: () => _onTapOption(context, details),
            last: dialogOptions.last == details,
          );
        }).toList(),
      ),
      actions: [
        CupertinoDialogAction(
          onPressed: () => _onCloseDialog(context),
          child: Text(
            closeAction.title,
            style: _closeTextStyle(),
          ),
        )
      ],
    );
  }

  Future<void> _onTapOption(
    BuildContext context,
    DialogOptionDetails option,
  ) async {
    Navigator.of(context).pop();

    if (option.preference != null) {
      final performanceService = await PerformanceService.instance();
      final speedService = await SpeedService.instance();

      await performanceService.updateQuality(option.preference!);
      await speedService.checkCurrentSpeed(
        timeoutMs: SpeedService.badResponseTime * 2,
      );
    }
  }

  _onCloseDialog(BuildContext context) {
    Navigator.of(context).pop();
  }

  TextStyle _titleTextStyle() => const TextStyle(
        fontSize: 17,
        color: ColorService.fontPrimary,
      );

  TextStyle _closeTextStyle() => const TextStyle(
        fontSize: 15,
        color: ColorService.accentSecondary,
      );

  static DialogOptionDetails closeOption(QualityPreference? preference) =>
      DialogOptionDetails(
        title: 'close',
        description: [],
        icon: MdiIcons.close,
        iconColor: ColorService.accentSecondary,
        height: defaultHeightDialogOption,
        preference: preference,
      );

  static List<DialogOptionDetails> defaultOptions() => [
        DialogOptionDetails(
          preference: QualityPreference.high,
          title: 'high quality',
          description: ['might be slow', 'requires high internet speed'],
          icon: MdiIcons.runFast,
          iconColor: ColorService.fontPrimary,
          height: defaultHeightDialogOption,
        ),
        DialogOptionDetails(
          preference: QualityPreference.medium,
          title: 'medium quality',
          description: ['medium performance'],
          icon: MdiIcons.bikeFast,
          iconColor: ColorService.fontPrimary,
          height: defaultHeightDialogOption,
        ),
        DialogOptionDetails(
          preference: QualityPreference.low,
          title: 'low quality',
          description: ['good performance'],
          icon: MdiIcons.truckFast,
          iconColor: ColorService.fontPrimary,
          height: defaultHeightDialogOption,
        ),
      ];
}
