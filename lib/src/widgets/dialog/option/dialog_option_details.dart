import 'package:flutter/material.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';

class DialogOptionDetails {
  DialogOptionDetails({
    required this.preference,
    required this.title,
    required this.description,
    required this.icon,
    required this.iconColor,
    required this.height,
    this.adjustFontToIconColor = false,
  });

  final QualityPreference? preference;
  final String title;
  final List<String> description;
  final IconData icon;
  final Color iconColor;
  final double height;
  final bool adjustFontToIconColor;
}
