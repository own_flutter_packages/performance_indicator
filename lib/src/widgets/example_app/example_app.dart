import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../performance_indicator.dart';

class ExampleApp extends StatelessWidget {
  static const String exampleContentId = 'asdf';

  const ExampleApp({
    Key? key,
    required this.width,
    required this.height,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    final Size size = Size(width, height);
    final double heightMediaContainer = size.height * 0.2;
    final double heightContentWithQualityConverter = size.height * 0.2;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final quality = await PerformanceIndicator.getCurrentQualityPreference();
      if (quality != null) {
        /// You can retrieve the stored information about quality preference by content_id.
        final content = await PerformanceIndicator.getContent(exampleContentId);

        if (content == null) {
          /// You can add information about the quality preference of your content.
          await PerformanceIndicator.addContent(
            exampleContentId,
            quality,
          );
        }
      }

      /// You can start current speed check.
      /// This will influence the color of performance indicator pulsing button.
      await PerformanceIndicator.checkCurrentSpeed(
        badResponseTime: 500,
        url: 'https://google.com',
      );
    });

    return Container(
      width: width,
      height: height,
      color: Colors.black12,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Center(
            /// This container could be your media content.
            child: Container(
              height: heightMediaContainer,
              width: size.width * 0.75,
              color: Colors.black38,
              alignment: Alignment.topRight,
              padding: const EdgeInsets.all(10),

              /// Quality converter can be placed inside of your media widget.
              ///
              /// Here exampleContentId is used. Please add content manually as shown in WidgetsBinding function above.
              child: PerformanceIndicator.qualityConverter(
                width: size.width * 0.1,
                contentId: exampleContentId,
                onChangedQuality: _onChangedContentQuality,
              ),
            ),
          ),
          Center(
            /// This is a content widget with a build-in quality converter.
            ///
            /// You can align quality converter relatively to your content.
            /// If you do not specify any content_id it will be generated.
            child: PerformanceIndicator.contentWithQualityConverter(
              child: Container(
                height: heightContentWithQualityConverter,
                width: size.width * 0.75,
                color: Colors.black54,
              ),
              qualityConverterWidth: size.width * 0.1,
              qualityConverterAlignment: const Alignment(0.9, 0.9),
              onCreated: _onCreatedContentId,
              onChangedContentQuality: _onChangedContentQuality,
            ),
          ),
          Expanded(
            child: Center(
              /// Performance indicator can be used placed everywhere
              /// as e.g. an already defined with quality dialog options.
              child: PerformanceIndicator.quality(
                width: size.width * 0.35,
                checkUri: Uri(host: 'google.com', scheme: 'https'),
                badResponseTimeInMs: 1000,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _onCreatedContentId(String contentId) {
    if (kDebugMode) {
      print('Created content ID $contentId automatically.');
    }
  }

  _onChangedContentQuality(
      String contentId, QualityPreference qualityPreference) {
    if (kDebugMode) {
      print('Updated content ID $contentId with quality: $qualityPreference.');
    }
  }
}
