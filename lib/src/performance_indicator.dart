part of performance_indicator;

/// The main API interface of PerformanceIndicatorInterface. Available through the `PerformanceIndicator` constant.
abstract class PerformanceIndicatorInterface {
  ExampleApp exampleApp(Size size);

  /// Returns performance indicator widget with custom dialog options.
  ///
  /// The indicator is a pulsing button that changes its color based on quality/speed calculation.
  ///
  /// If dialog options parameter is provided you can set custom dialog options for shown dialog.
  /// If checkUrl parameter is provided the custom url will be taken for check.
  /// If badResponseTime parameter is provided the speed quality will be calculated by custom time.
  /// If title parameter is provided the custom title will be displayed on dialog box.
  PerformanceIndicatorWidget custom({
    required double width,
    required List<DialogOptionDetails> dialogOptions,
    int? badResponseTimeInMs,
    Uri? checkUrl,
    String? title,
  });

  /// Returns performance indicator widget with default quality dialog options.
  ///
  /// The indicator is a pulsing button that changes its color based on quality/speed calculation.
  ///
  /// If checkUrl parameter is provided the custom url will be taken for check.
  /// If badResponseTime parameter is provided the speed quality will be calculated by custom time.
  /// If title parameter is provided the custom title will be displayed on dialog box.
  PerformanceIndicatorWidget quality({
    required double width,
    int? badResponseTimeInMs,
    Uri? checkUri,
    String? title,
  });

  /// Returns quality converter widget to indicate unexpected quality for content.
  ///
  /// Useful when user changes quality preferences and you want to enable user to reload content with current quality preference.
  /// The indicator is a pulsing button that is hidden as long as downloaded quality of content is the same as the current quality preference.
  QualityConverter qualityConverter({
    required double width,
    required String contentId,
    String? title,
    Function(String, QualityPreference)? onChangedQuality,
  });

  /// Returns content with quality converter.
  ///
  /// Useful when you want to receive updates for unexpected quality on your content widget.
  ContentWithChangingQuality contentWithQualityConverter({
    required Widget child,
    String? contentId,
    Alignment qualityConverterAlignment = Alignment.topRight,
    double? qualityConverterWidth,
    String? qualityConverterTitle,
    Function(String)? onCreated,
    Function(String, QualityPreference)? onChangedContentQuality,
  });

  /// Checks current speed by a request to provided URL and additionally by a ping to 8.8.8.8.
  ///
  /// After the check the response time for provided URL and for the ping will be calculated to a percentage.
  /// Ping response time has only 40% weight on final calculation result.
  ///
  /// If request parameter is provided the result will be the return of request.
  /// If url parameter is provided the custom url will be taken for check.
  /// If badResponseTime parameter is provided the speed quality will be calculated by custom time.
  /// If timeoutMs parameter is provided the request will abort after milliseconds given.
  Future<T?> checkCurrentSpeed<T>({
    Future<T>? request,
    String? url,
    int? badResponseTime,
    int? timeoutMs,
  });

  /// Add content to enable user to adjust quality when content has unexpected quality.
  ///
  /// A recording is created so the quality_converter can indicate if the current quality is unexpected.
  Future<ContentRecording> addContent(
    String contentId,
    QualityPreference qualityPreference, {
    bool? notAccessible = false,
  });

  /// Retrieves content recording with stored quality and timestamp.
  ///
  /// If no recording exists the result will be null.
  Future<ContentRecording?> getContent(String contentId);

  /// Updates content quality.
  ///
  /// If no recording exists a new recording will be created.
  Future<ContentRecording> updatedContentQuality(
    String contentId,
    QualityPreference qualityPreference,
  );

  /// Marks content as not accessible.
  ///
  /// If no recording exists a new recording will be created.
  Future<bool> markContentAsNotAccessible(String contentId);

  /// Updates quality preference.
  ///
  /// If already a quality preference was set the old recording will be deleted.
  Future<void> updateQuality(QualityPreference preference);

  /// Adds further recording for current internet speed connection details.
  ///
  /// Old (longer than 60 minutes ago) will be deleted by a cron job.
  Future<void> updateCurrentSpeed(SpeedDetails speedDetails);

  /// Gets current set quality preference.
  ///
  /// If no quality was set the result will be null.
  Future<QualityPreference?> getCurrentQualityPreference();

  /// Gets current set quality preference without async.
  ///
  /// If no quality was set the result will be null.
  QualityPreference? getCurrentQualityPreferenceSync();

  /// Gets last internet speed connection details.
  ///
  /// If no check was done the result will be null.
  Future<SpeedDetails?> getLastCheckedSpeed();

  /// Checks if any quality preference was stored.
  ///
  /// If a quality preference was stored the result will be true.
  Future<bool> existsAnyQualityPreference();

  /// Removes recordings of internet speed connection details that are older than 60 minutes.
  Future<void> removeOldRecordings();

  /// Deletes specific recordings of internet speed connection details.
  Future<void> deleteRecording(SpeedDetails speedDetails);
}
