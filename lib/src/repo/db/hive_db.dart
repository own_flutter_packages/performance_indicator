import 'package:hive_flutter/hive_flutter.dart';
import 'package:performance_indicator/src/model/content_recording/content_recording.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/model/speed_details/speed_details.dart';
import 'package:performance_indicator/src/services/content_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/services/speed_service.dart';

class HiveDB {
  static final HiveDB _singleton = HiveDB._internal();

  // ignore: unused_element
  factory HiveDB._() {
    return _singleton;
  }

  HiveDB._internal();

  static Future<HiveDB> instance({Function(int)? before}) async {
    before != null ? before(-1) : {};

    await _initHive();
    
    before != null ? before(1) : {};
    return _singleton;
  }

  Box<dynamic> db(String dbName) {
    return Hive.box(dbName);
  }

  Box<T> dbWithType<T>(String dbName) {
    return Hive.box<T>(dbName);
  }

  static Future<void> registerHiveBox<T>({
    required String name,
    required TypeAdapter<T> adapter,
    bool withType = false,
  }) async {
    final typeId = adapter.typeId;

    if (!(Hive.isAdapterRegistered(typeId))) {
      if (withType) {
        Hive.registerAdapter<T>(adapter);
      } else {
        Hive.registerAdapter(adapter);
      }
    }

    if (!(Hive.isBoxOpen(name))) {
      if (withType) {
        await Hive.openBox<T>(name);
        return;
      }
      await Hive.openBox(name);
    }
  }

  static Future<void> _initHive() async {
    await Hive.initFlutter('performance_indicator');

    await registerHiveBox(
      name: PerformanceService.dbName,
      adapter: QualityPreferenceAdapter(),
    );

    await registerHiveBox<SpeedDetails>(
      name: SpeedService.dbName,
      adapter: SpeedDetailsAdapter(),
      withType: true,
    );

    await registerHiveBox<ContentRecording>(
      name: ContentService.dbName,
      adapter: ContentRecordingAdapter(),
      withType: true,
    );
  }
}
