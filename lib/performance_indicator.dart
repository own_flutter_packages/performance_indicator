/// Performance indicator is a widget to show if current speed connection is set correct to current quality preferences.
library performance_indicator;

import 'package:flutter/material.dart';
import 'package:performance_indicator/src/impl/performance_indicator_impl.dart';
import 'package:performance_indicator/src/model/content_recording/content_recording.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/model/speed_details/speed_details.dart';
import 'package:performance_indicator/src/widgets/content/content_with_changing_quality.dart';
import 'package:performance_indicator/src/widgets/dialog/option/dialog_option_details.dart';
import 'package:performance_indicator/src/widgets/performance_indicator/performance_indicator_widget.dart';
import 'package:performance_indicator/src/widgets/quality_converter/quality_converter.dart';

import 'src/widgets/example_app/example_app.dart';

export 'src/model/content_recording/content_recording.dart'
    show ContentRecording;
export 'src/model/quality_preference/quality_preference.dart'
    show QualityPreference;
export 'src/model/speed_details/speed_details.dart' show SpeedDetails;
export 'src/repo/db/hive_db.dart' show HiveDB;
export 'src/widgets/dialog/option/dialog_option_details.dart'
    show DialogOptionDetails;

part 'src/performance_indicator.dart';

/// Global constant to access PerformanceIndicator.
// ignore: non_constant_identifier_names
final PerformanceIndicatorInterface PerformanceIndicator =
    PerformanceIndicatorImpl();
