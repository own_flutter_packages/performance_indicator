## 0.0.5

* Refactoring 
* Changed response times

## 0.0.4

* Refactoring 
* Improved animation handling

## 0.0.3

* Added Quality converter for media

## 0.0.2

* Added MacOS support
* Added Web support

## 0.0.1

* Init version
