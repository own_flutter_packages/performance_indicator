Package to manage the speed connection quality of your app. When user has a bad internet connection the widget changes its color and starts to be implusive. Then, user can downgrade the quality for content that is downloaded during usage of the app.

## Source code
Source code is available on [GitLab](https://gitlab.com/own_flutter_packages/performance_indicator).

## Features

![#f03c15](https://placehold.co/15x15/77F2F2/77F2F2) ```for good connection and fast responses```

![#f03c15](https://placehold.co/15x15/FFD24A/FFD24A) ```for bad connection and slow responses```

- Colors give the user of your app the feeling about the speed/quality ratio
- Calculations for speed/quality ratio are done in background 
- It is possible to influence the calculations by defining a response time that should be seen as bad internet speed connection
- Speed and response times are stored in DB ([Hive - key-value database](https://pub.dev/packages/hive))
- You can update the stored current connection speed by yourself
- You can update the stored current quality preference by yourself
- It is possible to provide own options for the dialog where user can set the quality of content it wants to receive
- It is possible to provide own requests for connection speed calculations

| Pulsing button </br> indicating good connection | Pulsing button </br> indicating bad connection |
| --- | --- |
|  ![dialog options](https://gitlab.com/own_flutter_packages/performance_indicator/-/raw/master/screenshots/good_connection.png) | ![dialog options](https://gitlab.com/own_flutter_packages/performance_indicator/-/raw/master/screenshots/bad_connection.png)|


| Dialog </br> indicating good choice | Dialog </br> indicating bad choice |
| --- | --- |
|  ![dialog options](https://gitlab.com/own_flutter_packages/performance_indicator/-/raw/master/screenshots/good_connection_dialog.png) | ![dialog options](https://gitlab.com/own_flutter_packages/performance_indicator/-/raw/master/screenshots/bad_connection_dialog.png)|


## Getting started

- Add to ```pubspec.yaml```
- ```flutter pub get```

## Usage

### Pulsing button
- Add ```PerformanceIndicator.quality()``` or ```PerformanceIndicator.custom()``` anywhere where you want it in your app
- Provide parameter ```width``` (required) for the pulsing button
- Provide parameter ```checkUri``` to check the specific URL to measure the internet speed
- Provide parameter ```badResponseTimeInMs``` to control the calculation of speed quality

```dart
MaterialApp(
    title: 'Example for performance indicator',
    home: Center(
        child: PerformanceIndicator.quality(
            width: 100.0,
            checkUri: Uri(host: 'google.com', scheme: 'https'),
            badResponseTimeInMs: 150,
        ),
    ),
    themeMode: ThemeMode.dark,
    theme: theme,
    color: Colors.black,
)
```
### Control currently stored values about connection speed and quality preferences by yourself
To start a speed check by yourself you can use ```PerformanceIndicator.checkCurrentSpeed()```  
```dart
await PerformanceIndicator.checkCurrentSpeed(
  badResponseTime: 500,
  url: 'https://google.com',
);
```
To provide own measured response time by yourself you can use ```PerformanceIndicator.updateCurrentSpeed()```  
```dart
await PerformanceIndicator.updateCurrentSpeed(
    SpeedDetails.of(
        responseTimeUri: uriTime,
        responseTimePing: pingTime,
        checkUrl: uri.toString(),
        qualityPreference: selectedQuality ?? QualityPreference.high,
        badResponseTime: badResponseTime,
    ),
);
```
To provide own status about saved quality preference by yourself you can use```PerformanceIndicator.updateQuality()```  
```dart
await PerformanceIndicator.updateQuality(QualityPreference.high);
```
To get current stored quality preference you can use ```PerformanceIndicator.getCurrentQualityPreference()```
Sometimes you need the quality preferences in non-async context. Therefore please use ```PerformanceIndicator.getCurrentQualityPreferenceSync()``` 
```dart
await PerformanceIndicator.getCurrentQualityPreference();
await PerformanceIndicator.getCurrentQualityPreferenceSync();
```
To get last checked speed connection details you can use ```PerformanceIndicator.getLastCheckedSpeed()```  
```dart
await PerformanceIndicator.getLastCheckedSpeed();
```

## License
```
Copyright 2022 Ilja Lichtenberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

End license text.
```